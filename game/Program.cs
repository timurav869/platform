﻿using System;
using System.Threading;

namespace game
{
    class Program
    {
        private static World world;
        delegate void buttons_fun();
        private static void start()
        {
            Pit pit = new Pit(20, 25);
            Pit[] entry = { pit };
            Map map = new Map(100, 20, entry);
            Player player = new Player(50);
            Enemies enemies = new Enemies(10);
            NPC npc = new NPC(15);
            Creatures[] creatures = { enemies, npc };
            world = new World(player, map, creatures);
            world.display();
        }
        private static void exit()
        {
            Console.WriteLine("Кнопочка");
            
        }
        //дмитрий игоревич
        static void Main(string[] args)
        {
            buttons_fun butt_fun = start;
            Button button_start = new Button(butt_fun, "Start",'*','#',5,20);
            butt_fun = exit;
            Button button_exit = new Button(butt_fun, "Exit", '*', '#', 5, 20);
            Button[] buttons = { button_start, button_exit };
            UiInterface ui = new UiInterface(buttons);
            ui.display();
        }

    }
    class Button
    {
        Delegate action;
        public char[] text { set; get; }
        public char badge { set; get; }
        public char invert_badge { set; get; }
        public int height { set; get; }
        public int wight { set; get; }
        public Button(Delegate action, string text, char badge, char invert_badge, int height, int wight)
        {
            this.action = action;
            this.text = text.ToCharArray();
            this.badge = badge;
            this.invert_badge = invert_badge;
            this.height = height;
            this.wight = wight;

        }
        public Button(Delegate action, string text, char badge, int wight)
        {
            this.action = action;
            this.text = text.ToCharArray();
            this.badge = '*';
            this.invert_badge = '#';
            this.height = height;
            this.wight = wight;

        }
        public void actionButton()
        {
            action.DynamicInvoke();
        }


    }
    
    class UiInterface
    {
        private Button[] buttons;
        private int point;
        public int wight { set; get; }
        public char badge { set; get; }
        public UiInterface(Button[] buttons)
        {
            point = 0;
            wight = 100;
            badge = ':';
            this.buttons = buttons;
        }
        private string StringСonclusion()
        {
            string render = new String(badge, wight) + "\n";
            for (int poi = 0; poi < buttons.Length; poi++)
            {
                string button_render = "";
                for (int i = 0; i < buttons[poi].height; i++)
                {
                    string string_render = "";
                    for (int j = 0; j < wight; j++)
                    {
                        
                        if (j < (wight - buttons[poi].wight) / 2 || j> (wight - buttons[poi].wight) / 2 + buttons[poi].wight)
                        {
                            string_render += badge;
                        }
                        else
                        {
                            //(j < (wight- buttons[poi].wight)/2 + (buttons[poi].wight - buttons[poi].text.Length) / 2 || j > (wight - buttons[poi].wight) / 2 + (buttons[poi].wight - buttons[poi].text.Length) / 2 + buttons[poi].text.Length)
                            if (i != buttons[poi].height/2)
                            {
                                if (poi == point)
                                {
                                    string_render += buttons[poi].invert_badge;
                                }
                                else
                                {
                                    string_render += buttons[poi].badge;
                                }
                            }
                            else
                            {
                                if ((j <= (wight - buttons[poi].wight) / 2 + (buttons[poi].wight - buttons[poi].text.Length) / 2 || j > (wight - buttons[poi].wight) / 2 + (buttons[poi].wight - buttons[poi].text.Length) / 2 + buttons[poi].text.Length))
                                {
                                    if (poi == point)
                                    {
                                        string_render += buttons[poi].invert_badge;
                                    }
                                    else
                                    {
                                        string_render += buttons[poi].badge;
                                    }
                                }
                                else
                                {
                                    string_render += buttons[poi].text[j - ((wight - buttons[poi].wight) / 2 + (buttons[poi].wight - buttons[poi].text.Length) / 2 )-1];
                                }
                                
                            }
                            
                            
                        }
                        
                    }
                    button_render += string_render + "\n";


                }
                render += button_render;
                render += new String(badge, wight) + "\n";
            }
            return render;
        }
        public void display()
        {
            string render = StringСonclusion();
            Console.WriteLine(render);
            while (true)
            {
                ConsoleKeyInfo key;
                key = Console.ReadKey();
                if (key.Key == ConsoleKey.UpArrow && point != 0)
                {
                    point--;
                    render = StringСonclusion();
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine(render);


                }
                else if (key.Key == ConsoleKey.DownArrow && point != buttons.Length-1)
                {
                    point++;
                    render = StringСonclusion();
                    Console.SetCursorPosition(0, 0);
                    Console.WriteLine(render);
                }
                else if(key.Key == ConsoleKey.Enter)
                {
                    buttons[point].actionButton();
                    break;
                }
                

            }
        }

    }
    class World
    {
        //написать ещё всё
        private Player player;
        private Map map;
        
        private Creatures[] creatures;
        public Creatures[] mask { set; get; } //маска карты с персонами
        public World(Player player, Map map, Creatures[] creatures)
        {

            this.player = player;
            this.map = map;
            this.creatures = creatures;
            

        }
        public World(Player player, Map map)
        {
            this.player = player;
            this.map = map;
            


        }
        public void display()
        {
            string map_string = pender();
            ConsoleKeyInfo key;
            Console.SetCursorPosition(0, 0);
            Console.WriteLine(map_string);
            while (true)
            {
                if (player.height != 0)
                {

                    if (player.height != player.height_jamp)
                    {

                        player.height++;


                    }
                    else
                    {
                        player.height *= -1;
                    }


                }

                if (map.map[player.x] == null && player.height == 0)
                {
                    Console.Clear();
                    Console.WriteLine("Ты упал");
                    
                    break;
                }
                key = Console.ReadKey(true);
                if (key.Key == ConsoleKey.RightArrow && player.x != map.map.Length)
                {

                    player.x++;

                    if (mask[player.x] != null && player.height == 0)
                    {

                        mask[player.x].meeting(player);
                    }


                }
                else if (key.Key == ConsoleKey.LeftArrow && player.x != 0)
                {
                    player.x--;
                    if (mask[player.x] != null && player.height == 0)
                    {
                        mask[player.x].meeting(player);
                    }


                }
                else if (key.Key == ConsoleKey.UpArrow && player.height == 0)
                {
                    player.height = 1;

                }


                map_string = pender();
                Console.SetCursorPosition(0, 0);
                Console.WriteLine(map_string);
            }
        }
        private string pender()
        {
            gen_mask();
            string answer = "";
            for(int i = 0; i <= map.height; i++)
            {
                string string_display = "";
                if (i == map.height / 2)
                {
                    
                    for (int j = 0; j<mask.Length; j++)
                    {
                        if(mask[j] != null)
                        {
                            string_display += mask[j].badge;
                        }
                        else
                        {
                            string_display += ':';
                        }
                        
                    }
                }
                else if(i == map.height / 2 - Math.Abs(player.height) && player.height != 0)
                {
                    for (int j = 0; j < mask.Length; j++)
                    {
                        if (j == player.x)
                        {
                            string_display += player.badge;
                        }
                        else
                        {
                            string_display += ':';
                        }

                    }
                }
                else if(i == map.height / 2 + 1)
                {
                    for (int j = 0; j < map.map.Length; j++)
                    {
                        if (map.map[j] != null)
                        {
                            string_display += map.map[j].badge;
                        }
                        else
                        {
                            string_display += ':';
                        }

                    }
                }
                else
                {
                    string_display = new String(':', map.x);
                    
                }
                answer += string_display + "\n";
            }
            return answer;
        }
        private void gen_mask()
        {
            
            int x = map.x;
            mask = new Creatures[x];
            
            if (creatures!=null)
            {
                foreach(Creatures entry in creatures)
                {
                    if (entry != null)
                    {
                        if (map.map[entry.x] != null && entry.HitPoints != 0)
                        {
                            mask[entry.x] = entry;
                        }
                    }
                }
                
            }
            if(player.height == 0)
            {
                mask[player.x] = player;
            }
        }
        
    }
    class Wall
    {
        public char badge { set; get; }
        public Wall(char badge)
        {
            this.badge = badge;
        }
    }
    class Map
    {
        public int height { set; get; }
        public int x { get; set; }
        private Pit[] pits;
        
        public Wall[] map { set; get; }
        public Map(int x, int height)
        {
            this.x = x;
            this.height = height;
            this.map = gen_map();
        }
        public Map(int x, int height, Pit[] pits)
        {
            this.x = x;
            this.height = height;
            this.pits = pits;
            this.map = gen_map();
        }
        private Wall[] gen_map()
        {
            Wall[] map_preliminary = new Wall[x];
            for (int i = 0; i < x; i++)
            {
                map_preliminary[i] = new Wall('*');
            }

            if (pits != null)
            {
                foreach (Pit pit in pits)
                {
                    for (int i = pit.StartX; i <= pit.StopX; i++)
                    {
                        map_preliminary[i] = null;
                    }
                }
            }

            
            return map_preliminary;
        }
    }

    class Pit
    {
        public int StartX { set; get; }
        public int StopX { set; get; }
        public Pit(int StartX, int StopX)
        {
            this.StartX = StartX;
            this.StopX = StopX;
        }
    }

    abstract class Creatures
    {
        public char badge { set; get; }
        public string name { set; get; }
        public int HitPoints { set; get; }
        public int x { set; get; }

        public abstract void meeting(Player player);
        public Creatures(char badge, string name, int HitPoints, int x) 
        {
            this.badge = badge;
            this.name = name;
            this.HitPoints = HitPoints;
            this.x = x;
        }
        
    }
    class Player : Creatures
    {
        //написать дамаг и урон
        public int height { get; set; }
        public bool jump { get; set; }
        public int height_jamp { set; get; }
        public int hit { set; get; }
        public Player(char badge, string name, int HitPoints, int x,int hit, int height_jamp) :base(badge, name, HitPoints, x)
        {
            this.height_jamp = height_jamp;
            this.jump = false;
            this.hit = hit;
            this.height = 0;
        }
        public Player(int hit) : base('&', "Чебровек", 100, 0)
        {
            this.height_jamp = 4;
            this.jump = false;
            this.hit = hit;
            this.height = 0;
        }
        public override void meeting(Player player)
        {

        }

        

    }
    class Enemies : Creatures
    {
        //написать дамаг и урон
        public int hit { set; get; }
        public int dx { set; get; }
        public int moment_x { set; get; }
        private bool right = true;
        public Enemies(char badge, string name, int HitPoints, int x, int hit, int dx) : base(badge, name, HitPoints, x)
        {
            this.moment_x = x;
            this.hit = hit;
            this.dx = dx;
        }
        public Enemies(int HitPoints, int x, int hit, int dx) : base('₽', "вражина", HitPoints, x)
        {
            this.moment_x = x;
            this.hit = hit;
            this.dx = dx;
        }
        public Enemies(int x) : base('₽', "вражина", 100, x)
        {
            this.moment_x = x;
            this.hit = 1;
            this.dx = 1;
        }
        public override void meeting(Player player)
        {
            player.HitPoints -= hit;
            this.HitPoints -= player.hit;
            Console.Clear();
            Console.WriteLine("Сражение \nполучино урона:"+hit.ToString()+"\nНанесено уроно:"+player.hit.ToString());
            Console.ReadKey();


        }
        
    }
    class NPC : Creatures
    {
        public string textQuest { set; get; }
        public int moment_x { set; get; }
        
        public NPC(char badge, string name, int HitPoints, int x, string textQuest) : base(badge, name, HitPoints, x)
        {
            this.moment_x = x;
            this.textQuest = textQuest;
        }
        public NPC(string name, int x, string textQuest) : base('@', name, 100, x)
        {
            this.moment_x = x;
            this.textQuest = textQuest;
        }
        public NPC(int x) : base('@', "Старик", 100, x)
        {
            this.moment_x = x;
            this.textQuest = "Приветствую тебя путник";
        }
        public override void meeting(Player player)
        {
            Console.Clear();
            Console.WriteLine(textQuest);
            Console.ReadKey();


        }

    }
    
}
